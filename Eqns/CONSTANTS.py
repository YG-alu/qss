import sympy as sy

PI = 3.141592653589793238 # ...
H = 6.62607004 * 10**-34 
H_BAR = H/2*PI
I = sy.I
#I = complex(0, 1)
