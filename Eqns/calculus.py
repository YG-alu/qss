import sympy

def differentiate(f): # derivatives
  return sympy.diff(f)

def integrate(f):     # integrals
  return sympy.integrate(f)

def ODE(lhs, rhs, f, x):
  return sympy.dsolve(lhs - rhs, f(x))

def PDE(lhs, rhs):  # Verify this function
  return sympy.pdsolve(lhs - rhs)
