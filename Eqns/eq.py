import CONSTANTS as c
import calculus as calc
import sympy as sy

class TimeIndependent:
  
  """
  EΨ(x) = -ħ/2m * d²Ψ(x)/dx² + v(x)Ψ(x)
        = ĤΨ(x)
  Ĥ = -ħ/2m * d²/dx² + v(x)
  """

  def __init__(self, wave_function=None, vector=None, energy=None, mass=None, potential_energy=None, kinetic_energy=None) -> None:
    self.psi = wave_function
    self.vec = vector
    self.E = energy
    self.m = mass
    self.PE = potential_energy
    self.KE = kinetic_energy

  def solveForWaveFunction(self) -> sy.Eq:
    if self.psi != None:
      return self.psi
    elif self.KE != None and self.m != None:
      return -2*self.m*calc.integrate(calc.integrate(self.KE))/c.H_BAR
    elif self.KE != None and self.PE != None:
      return calc.PDE(calc.PDE((self.KE + self.PE)/(c.I * c.H_BAR), 1), 1)
    elif self.psi != None:
      return self.psi
    else:
      return None
  
  def solveForEnergy(self) -> float: 
    if self.E != None:
      return self.E
    else:
      return None
  
  def solveForMass(self) -> float: 
    if self.m != None:
      return self.m
    else:
      return None
  
  def solveForPE(self) -> sy.Eq: 
    if self.PE != None:
      return self.PE
    else:
      return None
  
  def solveForKE(self) -> sy.Eq: 
    if self.KE != None:
      return self.KE
    else:
      return None


class TimeDependent:
  
  """
  iħ * d²Ψ(x, t)/dt² = -ħ/2m * d²Ψ(x, t)/dx² + v(x, t)Ψ(x, t)
                     = ĤΨ(x)
  Ĥ = -ħ/2m * d²/dx² + v(x)
  """

  def __init__(self, wave_function=None, vector=None, time=None, energy=None, mass=None, potential_energy=None, kinetic_energy=None) -> None:
    self.psi = wave_function
    self.vec = vector
    self.t = time
    self.E = energy
    self.m = mass
    self.PE = potential_energy
    self.KE = kinetic_energy

  def solveForWaveFunction(self) -> sy.Eq: 
    if self.psi != None:
      return self.psi
    elif self.KE != None and self.m != None:
      return -2*self.m*calc.integrate(calc.integrate(self.KE))/c.H_BAR
    elif self.KE != None and self.PE != None:
      return calc.PDE(calc.PDE((self.KE + self.PE)/(c.I * c.H_BAR), 1), 1)
    elif self.psi != None:
      return self.psi
    else:
      return None
  
  def solveForMass(self) -> float: 
    if self.m != None:
      return self.m
    else:
      return None
  
  def solveForPE(self) -> sy.Eq: 
    if self.PE != None:
      return self.PE
    else:
      return None
  
  def solveForKE(self) -> sy.Eq: 
    if self.KE != None:
      return self.KE
    else:
      return None
  
